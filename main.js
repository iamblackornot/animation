function addcommas(x) {
    //remove commas
    retVal = x ? parseFloat(x.toString().replace(/,/g, '')) : 0;

    //apply formatting
    return retVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function preStart() {
    gsap.set(".device-frame", {
        backgroundColor: "#2a3039",
        borderColor: "#2a3039"
    });

    gsap.set(".notification-page", {
        backgroundColor: "#2a3039",
        opacity: 1
    });

    gsap.set(".instagram-page", {
        backgroundColor: "#2a3039",
        opacity: 0
    });

    gsap.set(icon3, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa"
    });

    gsap.set(content3, {
        color: "#f7f9fa"
    });
}

function step1() {
    let tl = gsap.timeline();

    gsap.set(icon2, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde"
    });

    tl.to(icon3, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(content3, {
        color: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(icon1, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(content1, {
        color: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(".message", {
        opacity: 0, 
        duration: 1,
        ease: "power4.out",
    }, 0);

    tl.set(".message", {
        y: 75
    }, 0.5);

    tl.to(".device-frame", {
        backgroundColor: "#f7f9fa",
        borderColor: "#d3dce0",
        duration: 1,
        ease: "power4.out"
    }, 0);

    tl.to(".notification-page", {
        backgroundColor: "#2a3039",
        opacity: 0,
        duration: 1,
        ease: "power4.out"
    }, 0);   



    tl.to(".instagram-page", {
        backgroundColor: "#f7f9fa",
        opacity: 1,
        duration: 1,
        ease: "power2.out"
    }, 0);


    //tl.add("start");

    tl.to(start, 1.5, {
        val: 90789,
        roundProps:"val",
        onUpdate:function(){
            view.innerHTML=addcommas(start.val);
    }}, 0, 'start');
        
    return tl;
}

function step2() {
    let tl = gsap.timeline();

    tl.to(icon1, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(content1, {
        color: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(icon2, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(content2, {
        color: "#f7f9fa",
        duration: 0.5,
    }, 0);

    let dur = 1.5;

    tl.to(picAfter,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(picBefore,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(contentAfter,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(contentBefore,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(".picture", {
        outlineColor: "rgba(238, 238, 238, 0.3)",
        duration: dur,
        //ease: "power2.out"
    }, 0);

    tl.to(".site-content", {
        outlineColor: "rgba(238, 238, 238, 0.4)",
        duration: dur,
        //ease: "power2.out"
    }, 0);

    return tl;
}

function step3() {
    let tl = gsap.timeline();

    tl.to(icon2, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(content2, {
        color: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(icon3, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(content3, {
        color: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(".instagram-page", {
        backgroundColor: "#2a3039",
        opacity: 0,
        duration: 0.5,
        ease: "power4.out"
    }, 0);

    tl.to(".device-frame", {
        backgroundColor: "#2a3039",
        borderColor: "#2a3039",
        duration: 0.5,
        ease: "power4.out"
    }, 0);


    tl.to(".notification-page", {
        backgroundColor: "#2a3039",
        opacity: 1,
        duration: 0.5,
        ease: "power4.out"
    }, 0);

    tl.to(".message", {
        opacity: 1,
        y: 0,
        duration: 1,
        ease: "power4.out",
        stagger: {
            grid: [1,4],
            from: 0,
            //ease: "power1.out",
            each: 0.13 
        } 
    }, 0.5);

    // tl.fromTo(".message", {
    //     opacity: 0, 
    //     y: 75, 
    //     },{
    //     opacity: 1,
    //     y: 0,
    //     duration: 1,
    //     ease: "power4.out",
    //     stagger: {
    //         grid: [1,4],
    //         from: 0,
    //         ease: "power1.out",
    //         each: 0.13 
    //     } 
    // }, 0.5);

    return tl;
}

var i3, icon3, content3,
    i2, icon2, content2,
    i1, icon1, content1,
    picAfter, picBefore,
    contentAfter,
    start, view, gtl;

document.addEventListener('DOMContentLoaded', function() {
    i1 = document.getElementsByClassName("step-1");
    icon1 = i1[0].getElementsByClassName("indicator-icon")[0];
    content1 = i1[0].getElementsByClassName("indicator-content")[0];

    i2 = document.getElementsByClassName("step-2");
    icon2 = i2[0].getElementsByClassName("indicator-icon")[0];
    content2 = i2[0].getElementsByClassName("indicator-content")[0];

    i3 = document.getElementsByClassName("step-3");
    icon3 = i3[0].getElementsByClassName("indicator-icon")[0];
    content3 = i3[0].getElementsByClassName("indicator-content")[0];

    picAfter = CSSRulePlugin.getRule(".instagram-page .picture-frame::after");
    picBefore = CSSRulePlugin.getRule(".instagram-page .picture-frame::before");

    contentAfter = CSSRulePlugin.getRule(".instagram-page .site-content::after");
    contentBefore = CSSRulePlugin.getRule(".instagram-page .site-content::before");

    start = { val: 0 };
    view = document.querySelector(".number");

    preStart();

    gtl = gsap.timeline({delay: 0, repeat: -1, repeatDelay: 1, paused: true});
    gtl.add(step1(), "+=0");
    gtl.add(step2(), "+=1");
    gtl.add(step3(), "+=1");

    gtl.play();

}, true);