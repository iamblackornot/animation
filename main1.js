//                        <img src="https://socialcaptain.imgix.net/icons/instagram-like-icon.png?auto=compress" height="20">

//                        <source srcset="https://socialcaptain.imgix.net/socialcaptain-instagram-bot.webp?auto=compress" alt="Instagram Photo" type="image/webp">
//                        <source srcset="https://socialcaptain.imgix.net/socialcaptain-instagram-bot.jpg?auto=compress" alt="Instagram Photo" type="image/jpeg">

function addcommas(x) {
    //remove commas
    retVal = x ? parseFloat(x.toString().replace(/,/g, '')) : 0;

    //apply formatting
    return retVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function step1() {
    let tl = gsap.timeline();

    tl.to(icon3, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(content3, {
        color: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(".message", {
        opacity: 0, 
        duration: 0.5,
        ease: "power4.out",
    }, 0);

    
    // tl.fromTo(".device-frame", {
    //     backgroundColor: "#2a3039",
    //     borderColor: "#2a3039",
    //     }, {
    //     backgroundColor: "#f7f9fa",
    //     borderColor: "#d3dce0",
    //     duration: 1,
    //     ease: "power4.out"
    // }, 0);

    tl.to(".device-frame", {
        backgroundColor: "#f7f9fa",
        borderColor: "#d3dce0",
        duration: 1,
        ease: "power4.out"
    }, 0);

    tl.to(".notification-page", {
        backgroundColor: "#2a3039",
        opacity: 0,
        duration: 1,
        ease: "power4.out"
    }, 0);   

////////

    tl.to(icon1, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa",
        duration: 0.5,
    });

    tl.to(content1, {
        color: "#f7f9fa",
        duration: 0.5,
    }, 0);

    // tl.fromTo(".instagram-page", {
    //     backgroundColor: "#2a3039",
    //     opacity: 0
    //     }, {
    //     backgroundColor: "#f7f9fa",
    //     opacity: 1,
    //     duration: 1,
    //     ease: "power2.out"
    // }, 0);

    tl.to(".instagram-page", {
        backgroundColor: "#f7f9fa",
        opacity: 1,
        duration: 1,
        ease: "power2.out"
    }, 0);

    // tl.fromTo(".device-frame", {
    //     backgroundColor: "#2a3039",
    //     borderColor: "#2a3039",
    //     }, {
    //     backgroundColor: "#f7f9fa",
    //     borderColor: "#d3dce0",
    //     duration: 1,
    //     ease: "power4.out"
    // }, 0);

    // tl.to(".notification-page", {
    //     backgroundColor: "#2a3039",
    //     opacity: 0,
    //     duration: 1,
    //     ease: "power4.out"
    // }, 0);

    tl.to(start, 1.5, {
        val: 90789,
        roundProps:"val",
        onUpdate:function(){
            view.innerHTML=addcommas(start.val);
    }}, 0);

    gsap.set(".message", {
        opacity: 0,
        y: 100,
    }, 0.5);

    // for(const m of messages)
    //     m.y = 100;

    // console.log(messages.style);
    //console.log(messages);

    // messages.style.transform = "translateY(100px)";
    // messages.style.opacity = 0;

    // console.log(messages.style.top);
        
    return tl;
}

function step2() {
    let tl = gsap.timeline();

    tl.to(icon1, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(content1, {
        color: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(icon2, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(content2, {
        color: "#f7f9fa",
        duration: 0.5,
    }, 0);

    let dur = 1.5;

    tl.to(picAfter,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(picBefore,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(contentAfter,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(contentBefore,{
        opacity: 1,  
        duration: dur,
        //ease: "power1.out",
    }, 0);

    tl.to(".picture", {
        outlineColor: "rgba(238, 238, 238, 0.3)",
        duration: dur,
        //ease: "power2.out"
    }, 0);

    tl.to(".site-content", {
        outlineColor: "rgba(238, 238, 238, 0.4)",
        duration: dur,
        //ease: "power2.out"
    }, 0);

    return tl;
}

function step3() {
    let tl = gsap.timeline();

    tl.to(icon2, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(content2, {
        color: "#a7bfde",
        duration: 0.5,
    }, 0);

    tl.to(icon3, {
        backgroundColor: "#f7f9fa",
        borderColor: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(content3, {
        color: "#f7f9fa",
        duration: 0.5,
    }, 0);

    tl.to(".instagram-page", {
        backgroundColor: "#2a3039",
        opacity: 0,
        duration: 0.5,
        ease: "power2.out"
    }, 0);

    tl.to(".device-frame", {
        backgroundColor: "#2a3039",
        borderColor: "#2a3039",
        duration: 0.5,
        ease: "power4.out"
    }, 0);


    // tl.fromTo(".notification-page", {
    //     backgroundColor: "#f7f9fa"
    //     }, {
    //     backgroundColor: "#2a3039",
    //     opacity: 1,
    //     duration: 0.5,
    //     ease: "power4.out"
    // }, 0);

    tl.to(".notification-page", {
        backgroundColor: "#2a3039",
        opacity: 1,
        duration: 0.5,
        ease: "power4.out"
    }, 0);

    tl.fromTo(".message", {
        opacity: 0, 
        y: 75, 
        },{
        opacity: 1,
        y: 0,
        duration: 1,
        ease: "power4.out",
        stagger: {
            grid: [1,4],
            from: 0,
            //ease: "power1.out",
            each: 0.13 
        } 
    }, 0.5);

    // tl.to(".message", {
    //     opacity: 1,
    //     y: 0,
    //     duration: 1,
    //     ease: "power4.out",
    //     stagger: {
    //         grid: [1,4],
    //         from: 0,
    //         //ease: "power1.out",
    //         each: 0.13 
    //     } 
    // }, 0.5);

    // if(firstCycle == true)
    //     gtl.add(stepTr(), "+=1");

    return tl;
}

function stepTr() {
    let tl = gsap.timeline();

    tl.to(icon3, {
        backgroundColor: "transparent",
        borderColor: "#a7bfde",
        duration: 0.5,
        }, 0);

        tl.to(content3, {
            color: "#a7bfde",
            duration: 0.5,
        }, 0);

        tl.to(".message", {
            opacity: 0, 
            duration: 0.5,
            ease: "power4.out",
        }, 0);

        
        tl.fromTo(".device-frame", {
            backgroundColor: "#2a3039",
            borderColor: "#2a3039",
            }, {
            backgroundColor: "#f7f9fa",
            borderColor: "#d3dce0",
            duration: 1,
            ease: "power4.out"
        }, 0);
    
        tl.to(".notification-page", {
            backgroundColor: "#2a3039",
            opacity: 0,
            duration: 1,
            ease: "power4.out"
        }, 0);   

    return tl;
}

function tlOnRepeat() {
    // let frame = document.querySelector(".device-frame");
    // frame.style.backgroundColor = "#2a3039";
    // frame.style.borderColor = "#2a3039";

    // let npage = document.querySelector(".notification-page");
    // npage.style.backgroundColor = "#2a3039";
    // npage.style.opacity = 1;

    // let ipage = document.querySelector(".instagram-page");
    // ipage.style.backgroundColor = "#2a3039";
    // ipage.style.opacity = 0;

    // icon3.style.backgroundColor = "#f7f9fa";
    // icon3.style.borderColor = "#f7f9fa";

    // firstCycle = false;
    // console.log("repeat");
    gtl.invalidate();
}

function tlOnStart() {
    // let tl = gsap.timeline();
    // tl.to(start, 1.5, {
    //     val: 90789,
    //     roundProps:"val",
    //     onUpdate:function(){
    //         view.innerHTML=addcommas(start.val);
    // }}, 0);

}

function tlOnComplete() {
    //firstCycle = false;
    // gtl.invalidate();
    // gtl.restart();
    //gtl.invalidate();

    // gsap.set(".message", {
    //     opacity: 0,
    //     y: "+=100",
    // }, 0.5);

    //gtl.restart();
    //console.log('restart');
}

var i3, icon3, content3,
    i2, icon2, content2,
    i1, icon1, content1,
    picAfter, picBefore,
    contentAfter,
    start, view,
    firstCycle = true, gtl;

document.addEventListener('DOMContentLoaded', function() {
    i1 = document.getElementsByClassName("step-1");
    icon1 = i1[0].getElementsByClassName("indicator-icon")[0];
    content1 = i1[0].getElementsByClassName("indicator-content")[0];

    i2 = document.getElementsByClassName("step-2");
    icon2 = i2[0].getElementsByClassName("indicator-icon")[0];
    content2 = i2[0].getElementsByClassName("indicator-content")[0];

    i3 = document.getElementsByClassName("step-3");
    icon3 = i3[0].getElementsByClassName("indicator-icon")[0];
    content3 = i3[0].getElementsByClassName("indicator-content")[0];

    picAfter = CSSRulePlugin.getRule(".instagram-page .picture-frame::after");
    picBefore = CSSRulePlugin.getRule(".instagram-page .picture-frame::before");

    contentAfter = CSSRulePlugin.getRule(".instagram-page .site-content::after");
    contentBefore = CSSRulePlugin.getRule(".instagram-page .site-content::before");

    //messages = document.getElementsByClassName("message");
    //messages = document.querySelector('.message')
    //messageStyle = getComputedStyle(document.querySelector('.message');

    start = { val: 0 };
    view = document.querySelector(".number");

    gtl = gsap.timeline({delay: 0, repeat: -1, repeatDelay: 0, paused: true, onRepeat: tlOnRepeat, onStart: tlOnStart, onComplete: tlOnComplete, smoothChildTiming: true });
    //gtl.add(step1(), "+=0");
    // gtl.add(step2(), "+=1");
    // gtl.add(step3(), "+=1");
    // gtl.add(step1(), "+=1");

    gsap.to(start, 1.5, {
        val: 90789,
        roundProps:"val",
        onUpdate:function(){
            view.innerHTML=addcommas(start.val);},
        onComplete: () => {gtl.progress(0).play();}
    }, 0).reverse();
    //gtl.progress(0).play();

    //tl.add(stepTr(), "+=1");
}, true);